//config.js
var path = require('path');

var OPTIONS = {
	targetDir: function (app) {
		return path.join(app.get('rootDir'), 'img');
	},
	read: {
		name: '<<Picture server(read)>>',
		port: 80,
		'default': 'default.jpg',
        sizeReg: /(\w+)-(\d+)-(\d+)\.(\w+)$/
	},
	save: {
		name: '<<Picture server(save)>>',
		port: 9990
	},
	mode: 'read',
	contentType: {
		'jpg': 'image/ipeg',
		'jpeg': 'image/jpeg',
		'gif': 'image/gif',
		'png': 'image/png'
	}
};

module.exports = function (rootDir, app) {
	app.set('rootDir', rootDir);
	var $ = require('underscore');
	$.each(OPTIONS, function (v, k) {
		app.set(k, typeof v === 'function' ? v(app) : v);
	});
	app.use(require('body-parser')());
};